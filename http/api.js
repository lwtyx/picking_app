import Request from "./http.js";

// import user from './user.js';


export default {
	// ...user,
	
	/**
	 * 拣货app登录
	 */
	sign(data){			//默认 为需要token  
		// data.notoken = true;	// 不需要添加token 就设置为 true
		return Request('adminpicking/Login/login',data,{});
	},
	/**
	 * 获取验证码
	 * @param {Object} data
	 */
	getCode(data){
		return Request('app/Code/code',data,{})
	},
	/**
	 * 拣货app列表
	 */
	cateList(data){	  
		return Request('adminpicking/Picking/info',data,{});
	},
	/**
	 * 拣货操作
	 */
	goodsDetails(data){	
		return Request('adminpicking/Picking/goods_details',data,{});
	},
	/**
	 * 拣货完成操作
	 */
	finishGoods(data){	
		return Request('adminpicking/Picking/update',data,{});
	},
	
	// 报损界面
	/**
	 * 获取报损商品
	 */
	getBreakageGoods(data){	
		return Request('adminpicking/Picking/goods_damage_info',data,{});
	},
	/**
	 * 报损提交
	 */
	submitBreakage(data){	
		return Request('adminpicking/Picking/goods_damage_bs',data,{});
	},
	/**
	 * 入库提交
	 */
	submitRuku(data){	
		return Request('adminpicking/Picking/goods_damage_rk',data,{});
	},
	/**
	 * 报损历史
	 */
	historyBs(data){	
		return Request('adminpicking/Picking/goods_damage_bs_list',data,{});
	},
	/**
	 * 入库历史
	 */
	historyRk(data){	
		return Request('adminpicking/Picking/goods_damage_rk_list',data,{});
	},
	/**
	 * 修改密码
	 */
	editPwd(data){	
		return Request('admin/Adminpwd/edit_jh',data,{});
	},
	/**
	 * 平台数量
	 */
	numberLeftList(data){	
		return Request('adminpicking/Picking/goods_left_list',data,{});
	},
	/**
	 * 删除
	 */
	deleteData(data){	
		return Request('adminpicking/Picking/delete',data,{});
	},
	/**
	 * 取消拣货
	 */
	cancelPicking(data){	
		return Request('adminpicking/Picking/cancelPick',data,{});
	},
	/**
	 * 检查订单状态
	 */
	cancelCheck(data){	
		return Request('adminpicking/Picking/checkCancel',data,{});
	},
	
	
	
	
	
	
	
	/**
	 * get请求方式
	 * @param {Object} data
	 */
	saasdf(data){			//默认 每个接口都需要token
		return Request('user/api_user_login',data,{},'get');
	},
	
	tost(title){
		return msg(title);
	},
	
	
}
