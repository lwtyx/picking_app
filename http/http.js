import tools from '../utils/tools.js'
import baseUrl from './helper.js'


let headers = {
	'Content-Type': 'application/json',
}

let urlPath = baseUrl.baseUrl


const Request = (url,data = {},dataheader = {},method = 'post')=>{
		uni.showLoading({
			title:'加载中'
		});		
		let token;
		let value = uni.getStorageSync('userInfo')
		if(value){
			token = {
				'token':value.token,
				'admin_id':value.admin_id
			}
		}else{
			// console.log('没有token')
		}
		
		const DATA = {
			...data,
			...token,
		}
		
		
		return new Promise((reslove,reject)=>{
			uni.request({				
				url:urlPath + url,				
				data:DATA,
				header: {
					...headers,
					...dataheader
				},
				method: method,
				success: (response) => {
					// console.log(response)
					reslove(response);
				},
				complete: () => {
					uni.hideLoading();
				},
				fail: (error) => {
					uni.hideLoading();
					reject(error);
				},
			});
		})
}



export default Request;




