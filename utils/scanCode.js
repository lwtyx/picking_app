// 商米红外线扫码配置
// 监听广播："com.sunmi.scanner.ACTION_DATA_CODE_RECEIVED"
const Net = "com.sunmi.scanner.ACTION_DATA_CODE_RECEIVED"
const DATA = "data";
const SOURCE = "source_byte";

export {
	Net,
	DATA,
	SOURCE
}
