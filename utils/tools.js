
export default {
	msg: (title, duration = 1500, mask = false, icon = 'none') => {
		//统一提示方便全局修改
		if (Boolean(title) === false) {
			return;
		}
		uni.showToast({
			title,
			duration,
			mask,
			icon
		});
	},
	// 取缓存
	get: (key) => {
		return uni.getStorageSync(key)
	},
	// 存缓存
	set: (key, value) => {
		uni.setStorageSync(key, value)
	},
	// 跳转页面
	go: (url) => {
		uni.navigateTo({
			url: url
		})
	},
	switchGo: (url) => {
		uni.switchTab({
			url: url
		})
	},
	reLaunch: (url) => {
		uni.reLaunch({
			url: url
		})
	},
	back: (num) => {
		uni.navigateBack({
			delta: num
		})
	},
	setSync: (key, data) => {
		uni.setStorageSync(key, data)
	},
	getSync: (key) => {
		uni.getStorageSync(key)
	},


	loading: () => {
		uni.showLoading({
			title: '请稍等。。。'
		})
	},
	closeLoading: () => {
		uni.hideLoading()
	},

	jump: () => {
		console.log('jump');
	},

	a1(data) {
		console.log(data)
	},
	

}
