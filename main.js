import Vue from 'vue'
import App from './App'
import tools from './utils/tools.js'
import reg from './utils/reg.js'
import apis from './http/api.js'

Vue.config.productionTip = false

Vue.prototype.$tools = tools;
Vue.prototype.$reg = reg;
Vue.prototype.$api = apis;

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
